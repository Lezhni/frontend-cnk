var hwSlideSpeed = 700;
var hwTimeOut = 3000;
var hwNeedLinks = false;
 
$(document).ready(function(e) {
    $('.slide').css(
        {"position" : "absolute",
         "top":'0', "left": '0'}).hide().eq(0).show();
    var slideNum = 0;
    var slideTime;
    slideCount = $("#slider .slide").size();
    var animSlide = function(arrow){
        clearTimeout(slideTime);
        $('.slide').eq(slideNum).fadeOut(hwSlideSpeed);
        if(arrow == "next"){
            if(slideNum == (slideCount-1)){slideNum=0;}
            else{slideNum++}
            }
        else if(arrow == "prew")
        {
            if(slideNum == 0){slideNum=slideCount-1;}
            else{slideNum-=1}
        }
        else{
            slideNum = arrow;
            }
        $('.slide').eq(slideNum).fadeIn(hwSlideSpeed, rotator);
        $(".link.active").removeClass("active");
        $('.link').eq(slideNum).addClass('active');
        }
    var pause = false;
    var rotator = function(){
    if(!pause){slideTime = setTimeout(function(){animSlide('next')}, hwTimeOut);}
            }
    $('#slider').hover(    
        function(){clearTimeout(slideTime); pause = true;},
        function(){pause = false; rotator();
        });
    rotator();

    $('.sl').click(function(){
        animSlide("next");
        return false;
    });
    $('.sr').click(function(){
        animSlide("prew");
        return false;
    });

    $('.slide1').css(
        {"position" : "absolute",
         "top":'0', "left": '0'}).hide().eq(0).show();
    var slideNum1 = 0;
    var slideTime1;
    slideCount1 = $("#slider-inner .slide1").size();
    var animSlide1 = function(arrow){
        clearTimeout(slideTime1);
        $('.slide1').eq(slideNum1).fadeOut(hwSlideSpeed);
        if(arrow == "next"){
            if(slideNum == (slideCount1-1)){slideNum1=0;}
            else{slideNum1++}
            }
        else if(arrow == "prew")
        {
            if(slideNum1 == 0){slideNum1=slideCount1-1;}
            else{slideNum1-=1}
        }
        else{
            slideNum1 = arrow1;
            }
        $('.slide1').eq(slideNum1).fadeIn(hwSlideSpeed, rotator1);
        }
    var pause1 = false;
    var rotator1 = function(){
    if(!pause){slideTime1 = setTimeout(function(){animSlide1('next')}, hwTimeOut);}
            }
    $('#slider-inner').hover(    
        function(){clearTimeout(slideTime1); pause1 = true;},
        function(){pause1 = false; rotator1();
        });
    rotator1();
});
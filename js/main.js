$(document).ready(function() {
	$("#feedback").magnificPopup({
		type: 'inline',
		preloader: 'false',
		removalDelay: 0,
	});
	$('.upload').upload();
	$('.menu-block').click(function() {
		if($(this).hasClass('opened')) {
			$(this).removeClass('opened').children('.firstlevel').slideUp();
		} else {
			$('.menu-block').removeClass('opened').children('.firstlevel').slideUp();
			$(this).addClass('opened').children('.firstlevel').slideDown();
		}
	});
	$('.secondlevel-link').click(function() {
		if($(this).hasClass('opened-second')) {
			$(this).removeClass('opened-second').next('.secondlevel').slideUp();
		} else {
			$(this).addClass('opened-second').next('.secondlevel').slideDown();
		}
		return false;
	});
});